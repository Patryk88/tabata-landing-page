import React from 'react';
import styled from 'styled-components';

import Header from '../components/header';
import Footer from '../components/footer';
import GoBackButton from '../components/backButton';

import { colors } from '../styles'

const Container = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  background-color: ${colors.white};
`;

const PageTexts = {
  heading: 'ERROR 404',
  about: `A problem occured, The link is broken, page has been moved. Let's go back!`,
}

const ErrorPage = () => {
  return (
    <Container>
      <Header headingText={PageTexts.heading} aboutText={PageTexts.about} GoBackButton={GoBackButton} errorPage={true}/>
      <Footer/>
    </Container>
  )
}

export default ErrorPage;