import React from 'react';

import styled from 'styled-components';

import Header from '../components/header';
import HowItWorksSection from '../components/howItWorksSection';
import PresentationSection from '../components/presentationSection';
import Footer from '../components/footer';

import { colors } from '../styles';

const PageTexts = {
  heading: 'Fast. Convenient. Affordable.',
  about: `Trainingo is a tabata timer which allows
          you to create your workout with easy to
          use interface and simple design`,
}

const Container = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  background-color: ${colors.white};
`;

const MainLayout = () => {
  return (
    <Container>
      <Header headingText={PageTexts.heading} aboutText={PageTexts.about} />
      <HowItWorksSection/>
      <PresentationSection/>
      <Footer/>
    </Container>
  )
}

export default MainLayout;