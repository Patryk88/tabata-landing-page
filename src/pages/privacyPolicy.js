import React from 'react';
import styled, { css } from 'styled-components';

import PageWithText from '../components/pageWithText';
import GoBackButton from '../components/backButton';

import { colors, sizes } from '../styles'
import { settings } from '../texts/texts.json';


const LinksStyling = css`
  a:last-child {
      padding-bottom: 4rem;
  }
`;

const StyledLinks = styled.a`
  color: ${colors.red};
  font-family: 'rubik-medium';
  font-size: ${sizes.medium};
  display: block;
  padding: .5rem 3rem;
  width: 22rem;

  :hover {
    text-decoration: underline;
  }
`;

const PageTexts = {
  heading: 'Privacy & policy',
  about: `Here are some infomrations about our privacy policy`,
}

const PrivacyPolicy = () => {
  return (
    <PageWithText headingText={PageTexts.heading} aboutText={PageTexts.about} GoBackButton={GoBackButton} customPage={true} LinksStyling={LinksStyling}>
      {settings.privacy1}
      <StyledLinks href="https://policies.google.com/privacy" target="_blank">googlePlayServices</StyledLinks>
      <StyledLinks href="https://support.google.com/admob/answer/6128543?hl=en" target="_blank">AdMob</StyledLinks>
      <StyledLinks href="https://firebase.google.com/policies/analytics" target="_blank">Firebase Analytics</StyledLinks>
      <StyledLinks href="#">Log Data</StyledLinks>
      {settings.privacy2}
    </PageWithText>
  )
}

export default PrivacyPolicy;