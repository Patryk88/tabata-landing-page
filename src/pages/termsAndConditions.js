import React from 'react';

import PageWithText from '../components/pageWithText';
import GoBackButton from '../components/backButton';

import { settings } from '../texts/texts.json';

const PageTexts = {
  heading: 'Terms & conditions',
  about: `Here are some informations about our terms and conditions`,
}

const TermsAndConditions = () => {
  return (
    <PageWithText headingText={PageTexts.heading} aboutText={PageTexts.about} GoBackButton={GoBackButton}>
      {settings.termsAndConditions}
    </PageWithText>
  )
}

export default TermsAndConditions;