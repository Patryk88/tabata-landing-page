export const colors = {
  white: '#fff',
  yellow: '#FDFF00',
  tYellow: 'rgba(253,255,0,.9)',
  grey: '#5F676C',
  red: '#F60957',
  green: '#2FD20E',
  blue: '#00E6FE',
  black: '#1C262F',
  tblack: 'rgba(28,38,47,.7)'
};

export const sizes = {
  small: '1.8rem',
  medium: '2.5rem',
  large: '4.5rem',
}
