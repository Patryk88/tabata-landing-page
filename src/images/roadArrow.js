import React from 'react'

const roadArrowSvg = props => (
  <svg xmlns='http://www.w3.org/2000/svg' viewBox="0 0 2584 768" {...props}>
    <path fill="none" d="M0 352L2548 0 129 626 0 352z" />
    <path fill="#f60957" d="M36 498l2548-156L166 768 36 498z" />
  </svg>
)

export default roadArrowSvg;
