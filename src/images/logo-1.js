import React from 'react'

const LogoSvg = props => (
  <svg viewBox="0 0 512 512" {...props}>
    <path
      d="M112 256a144 144 0 1 0 144-144"
      stroke="#fdff00"
      fill="none"
      strokeMiterlimit={10}
      strokeWidth={29.12}
    />
    <path
      d="M245.92 112A134.24 134.24 0 0 0 112 245.92"
      stroke="#fff"
      fill="none"
      strokeMiterlimit={10}
      strokeWidth={29.12}
    />
    <text
      transform="translate(196.96 331.51)"
      style={{
        isolation: 'isolate',
      }}
      fontSize={189.6}
      fill="#fdff00"
      fontFamily="Rubik-MediumItalic,Rubik"
      fontWeight={500}
      fontStyle="italic"
      letterSpacing="-.04em"
    >
      {'T'}
    </text>
  </svg>
)

export default LogoSvg;