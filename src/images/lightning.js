import React from 'react'

const LightningSvg = props => (
  <svg viewBox="0 0 400 400" {...props}>
    <title />
    <defs>
      <path
        d="M223.545 195.233h105.341c2.206 0 2.569 1.093.821 2.434l-195.654 150.13c-1.752 1.345-2.631.721-1.968-1.377l44.612-141.118H71.947c-2.206 0-2.593-1.126-.876-2.506l186.69-150.057c1.723-1.384 2.67-.765 2.122 1.365l-36.338 141.129z"
        id="prefix__88"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path
        d="M200 400c110.457 0 200-89.543 200-200S310.457 0 200 0 0 89.543 0 200s89.543 200 200 200z"
        fill="#00E6FE"
      />
      <use fill="#FBF0B8" xlinkHref="#prefix__88" />
    </g>
  </svg>
)

export default LightningSvg;
