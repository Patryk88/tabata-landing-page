import React from 'react'

const LandScapeSvg = props => (
  <svg viewBox="0 0 400 400" {...props}>
    <defs>
      <path id="prefix__8" d="M60 80h280v220H60z" />
      <path id="prefix__6" d="M255 150l65 110H190z" />
      <path id="prefix__4" d="M260 280H80l90-140z" />
      <circle cx={110} cy={115} id="prefix__2" r={25} />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path
        d="M200 400c110.457 0 200-89.543 200-200S310.457 0 200 0 0 89.543 0 200s89.543 200 200 200z"
        fill="#F60957"
      />
      <g>
        <use fill="#fff" xlinkHref="#prefix__6" />
      </g>
      <g>
        <use fill="#fff" xlinkHref="#prefix__4" />
      </g>
      <g>
        <use fill="#FDFF00" xlinkHref="#prefix__2" />
      </g>
    </g>
  </svg>
)

export default LandScapeSvg;
