import React from 'react'

const SuccessSvg = props => (
  <svg viewBox="0 0 400 400" {...props}>
    <defs>
      <path
        id="prefix__b"
        d="M170.137 244.25L314.387 100l28.284 28.284-172.534 172.534-14.142-14.142L57 187.681l28.284-28.284 84.853 84.853z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <ellipse cx={200} cy={200} fill="#2FD20E" rx={200} ry={200} />
      <use fill="#F2ECD2" xlinkHref="#prefix__b" />
    </g>
  </svg>
)

export default SuccessSvg;
