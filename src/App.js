import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import { createGlobalStyle } from 'styled-components';

import MainLayout from './pages/mainPage';
import PrivacyPolicy from './pages/privacyPolicy';
import TermsAndConditions from './pages/termsAndConditions';

import Error404 from './pages/error404';

import ResetStyles from './styles/resetStyles';

const GlobalStyle = createGlobalStyle`
  ${ResetStyles};
  margin: 0;
  padding: 0;
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
    "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
    sans-serif;
  html, body {
    font-size: 10px;
  }

  *,*::before,*::after {
    box-sizing: border-box;
    text-decoration: none;
  }
`;

const ScrollToTop = () => {
  window.scrollTo(0, 0);
  return null;
};

const App = () => {
  return (
    <Router>
      <Route component={ScrollToTop} />
      <GlobalStyle />
      <Switch>
        <Route exact path="/" component={MainLayout} />
        <Route path="/privacy-policy" component={PrivacyPolicy} />
        <Route path="/terms-and-conditions" component={TermsAndConditions} />
        <Route component={Error404} />
      </Switch>
    </Router>
  );
};

export default App;
