import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import { colors, sizes } from '../styles'


const BackButton = styled.div`
  width: 13rem;
  height: ${sizes.large};
  background-color: ${colors.black};
  border: .2rem solid ${colors.yellow};
  border-radius: ${sizes.medium};;
  padding: 1.1rem 0;
  text-align: center;
  margin: ${sizes.small};

  @media (min-width: 567px) {
    margin: 0;
  }

  a {
    font-family: 'rubik-bold-italic';
    margin: 0;
    font-size: ${sizes.small};
    text-align: center;
    text-transform: uppercase;
    color: ${colors.white};
    user-select: none;
  }
`;

export const GoBackButton = () => {
  return (
    <BackButton>
      <Link to="/">go back</Link>
    </BackButton>
  );
}

export default GoBackButton;