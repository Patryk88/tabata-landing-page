import React from 'react';
import Plx from 'react-plx';

import styled from 'styled-components';

import SuccessSvg from '../images/success';
import LandScapeSvg from '../images/landscape';
import LightningSvg from '../images/lightning';

import { colors, sizes } from '../styles';

const Container = styled.section`
  width: 87.5%;
  margin: 0 auto;
  padding: ${sizes.medium} 0;

  @media (min-width: 1060px) {
    min-height: 20rem;
    height: 60vh;
  }
`;

const ParallaxSection = styled(Plx)`
  z-index: 10;
`;

const Heading = styled.h2`
  font-family: 'rubik-bold-italic';
  font-size: ${sizes.large};
  text-align: center;
  text-transform: uppercase;
  color: ${colors.black};
  margin: ${sizes.large} 0;
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  @media (min-width: 567px) {
    flex-direction: row;
    align-items: flex-start;
  }
`;

const ContentBox = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  padding-top: ${sizes.large};

  @media (min-width: 567px) {
    width: 33.33%;
  }
`;

const Text = styled.h3`
  font-family: 'rubik-medium';
  font-size: ${sizes.medium};
  margin: ${sizes.small} 0;
  text-align: center;
  color: ${colors.black};
`;

const AboutText = styled.p`
  font-family: 'rubik-regular';
  font-size: ${sizes.small};
  text-align: center;
  color: ${colors.black};
  width: 60%;

  @media (min-width: 567px) {
    width: 80%;
  }

  @media (min-width: 720px) {
    width: 60%;
  }

  @media (min-width: 1024px) {
    width: 40%;
  }
`;

const SvgWidth = 92;

const parallaxData = [
  {
    start: ".parallax-1",
    end: ".parallax-2",
    properties: [
      {
        startValue: 200,
        endValue: 0,
        property: 'translateY',
      },
      {
        startValue: 0,
        endValue: 1,
        property: 'opacity',
      },
    ],
  },
];

const HowItWorksSection = () => {
  return (
    <ParallaxSection parallaxData={ parallaxData }>
      <Container className="parallax-1">
        <Heading>
          Why trainingo?
        </Heading>
        <ParallaxSection parallaxData={ parallaxData }>
          <Content>
            <ContentBox>
              <SuccessSvg width={ SvgWidth }/>
              <Text>
                Easy to use
              </Text>
              <AboutText>
                We have designed our interface with care, so it's easy to use
              </AboutText>
            </ContentBox>
            <ContentBox>
              <LandScapeSvg width={ SvgWidth }/>
              <Text>
                Beautiful design
              </Text>
              <AboutText>
                We worked hard to deliver you best visual experience
              </AboutText>
            </ContentBox>
            <ContentBox>
              <LightningSvg width={SvgWidth}/>
              <Text>
                lightning fast
              </Text>
              <AboutText>
                Super fast even on low-and devices
              </AboutText>
            </ContentBox>
          </Content>
        </ParallaxSection>
      </Container>
    </ParallaxSection>
  )
}

export default HowItWorksSection;