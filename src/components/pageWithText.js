import React from 'react';
import styled from 'styled-components';

import Header from '../components/header';
import Footer from '../components/footer';

import { colors, sizes } from '../styles'

const Container = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  background-color: ${colors.white};
`;


const StyledHeader = styled.h2`
  color: ${colors.white};
  text-transform: uppercase;
  font-family: 'rubik-bold-italic';
  margin: ${sizes.medium} 0;
  font-size: 2rem;
`;

const StyledContent = styled.p`
  color: ${colors.white};
  white-space: pre-wrap;
  font-family: 'rubik-regular';
  font-size: 1.8rem;
  ${({LinksStyling}) => LinksStyling};
`;

const Content = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  background-color: ${colors.black};
  margin: 0 auto;
  padding: ${sizes.medium};

  @media (min-width: 720px) {
    width: 87.5%;
    padding: 10rem;
    margin: 5rem auto;
  }
`;

const PageWithText = ({ children, headingText, aboutText, GoBackButton, customPage, LinksStyling }) => {
  return (
    <Container>
      <Header headingText={headingText} aboutText={aboutText} GoBackButton={GoBackButton} customPage={customPage}/>
      <Content>
          <StyledHeader> {headingText} </StyledHeader>
          <StyledContent LinksStyling={LinksStyling}>
            {children}
          </StyledContent>
      </Content>
      <Footer/>
    </Container>
  )
}

export default PageWithText;