import React from 'react';

import styled from 'styled-components';

import { links } from '../texts/texts.json';

import LogoSvg from '../images/logo-1';

import { colors, sizes } from '../styles'

const Background = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  padding: ${sizes.medium} 6.25%;
  background-color: ${colors.black};
  background-image: url('../../images/bgimage.png');
  background-position: center center;
  background-repeat: no-repeat;
  background-size: cover;

  @media (min-width: 720px) {
    flex-direction: row;
    align-items: flex-start;
  }

  @media (min-width: 813px) {
    padding: ${sizes.large} 0 0 0;
    width: 87.5%;
    margin: 0 auto;
    background: none;
  }

  @media (min-width: 1060px) {
    height: 100vh;
  }

`;

const BackgroundColor = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;

  @media (min-width: 813px) {
    min-height: 61rem;
    height: calc(100vh - 10rem);
    background-color: ${colors.black};
    background-image: url('../../images/bgimage.png');
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
  }

  @media (min-width: 1024px) and (max-height: 1366px) and (orientation:portrait) {
    height: 61rem;
  }
`;

const Brand = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  z-index: 1;
`;

const BrandName = styled.h2`
  color: ${colors.yellow};
  text-transform: uppercase;
  font-family: 'rubik-bold-italic';
  font-size: ${sizes.small};
`;

const Phones = styled.div`
  width: 32rem;
  height: 45rem;
  background-image: url('../../images/phones.png');
  display: block;
  background-size: contain;
  background-repeat: no-repeat;
  z-index: 1;

  @media (min-width: 720px) {
    width: 30rem;
    height: 60rem;
    background-image: url('../../images/phones-2.png');
    display: block;
  }

  @media (min-width: 640px) and (max-height: 360px) {
    width: 30rem;
    height: 53rem;
  }

  @media (min-width: 1060px) {
    width: 42rem;
    background-image: url('../../images/phones.png');
    display: block;
  }
`;

const Content = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  @media (min-width: 720px) {
    justify-content: flex-start;
    align-items: flex-start;
    position: absolute;
    left: 16%;
    ${({errorPage}) => errorPage ? 'top: 20vh' : 'top: 5%'};
    ${({ customPage }) => customPage ? 'top: 20vh' : null};
  }

  @media (min-width: 1060px) {
    ${({errorPage}) => errorPage ? 'top: 20vh' : 'top: 8%'};
    ${({ customPage }) => customPage ? 'top: 20vh' : null};
  }

`;

const Heading = styled.h1`
  font-family: 'rubik-bold-italic';
  font-size: ${sizes.large};
  color: ${colors.yellow};
  margin: ${sizes.small} 0;
  width: min-content;
  ${({ customPage }) => customPage ? 'width: 100%' : 'width: min-content'};

  @media (min-width: 720px) {
    width: min-content;
    text-align: left;
  }
  @media (min-width: 813px) {
    ${({ customPage }) => customPage ? 'width: 100%' : 'width: min-content'};
  }
`;

const About = styled.h2`
  font-family: 'rubik-regular';
  font-size: ${sizes.small};
  color: ${colors.white};
  align-self: flex-start;
  max-width: 80%;

  @media (min-width: 720px) {
    max-width: 40%;
    margin-bottom: ${sizes.small};
  }
`;

const StyledLink = styled.a`
  width: 20rem;
`;

const GooglePlayButton = styled.div`
  width: 20rem;
  height: 7.5rem;
  background-image: url('../../images/google-play-badge.png');
  background-size: cover;
  background-repeat: no-repeat;
`;

class Header extends React.Component {
  render () {
    const { headingText, aboutText, GoBackButton, errorPage, customPage } = this.props;

    return (
      <Background>
        <BackgroundColor/>
        <Brand>
          <LogoSvg width={72} />
          <BrandName>Trainingo</BrandName>
        </Brand>
          <Phones/>
        <Content customPage={customPage} errorPage={errorPage}>
          <Heading customPage={customPage}>
            {headingText}
          </Heading>
          <About>
            {aboutText}
          </About>
          { GoBackButton ?
            <GoBackButton/> :
            <StyledLink href={links.googlePlay} target="_blank" rel="noopener noreferrer">
              <GooglePlayButton />
            </StyledLink>
          }
        </Content>
      </Background>
    );
  }
}

export default Header;