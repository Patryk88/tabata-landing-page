import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import Plx from 'react-plx';

import styled from 'styled-components';

import RoadArrowSvg from '../images/roadArrow';

import { colors, sizes } from '../styles';

const ParallaxSection = styled(Plx)`
  z-index: 10;
`;

const Heading = styled.h2`
  font-family: 'rubik-bold-italic';
  font-size: ${sizes.large};
  text-transform: uppercase;
  text-align: center;
  color: ${colors.black};
  margin: ${sizes.large} 0;

  @media (min-width: 720px) {
    text-align: left;
  }
`;

const BackgroundImage = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
  background-repeat: no-repeat;
  background-size: cover;
  width: 87.5%;
  margin: 0 auto;
  overflow: hidden;
  padding: ${sizes.medium} 0;

  @media (min-width: 720px) {
    background: ${({ svgImage }) => svgImage};
    flex-direction: row;
    margin-left: -20%;
    background-size: 120% 120%;
    width: 120%;
    height: 100vh;
  }
`;

const Content = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  width: 100%;

  @media (min-width: 720px) {
    width: 50%;
    margin-left: 20%;
  }
`;

const About = styled.p`
  font-family: 'rubik-regular';
  font-size: ${sizes.medium};
  color: ${colors.black};
  margin: ${sizes.small} auto;

  @media (min-width: 720px) {
    margin: 0;
    min-width: 30%;
    max-width: 70%;
  }
`;

const Images = styled.div`
  position: relative;
  height: 54rem;
  width: 28rem;
`;

const PhoneFrame = styled.img`
  position:absolute;
  left: 0;
  width: 28rem;
  height: 54rem;
  margin-bottom: 15rem;
  overflow: hidden;

  @media (min-width: 567px) {
    background-size: contain;
    margin-bottom: 0;
  }
`;

const Phone = styled.img`
  z-index: 20000;
  position:absolute;
  left: 2rem;
  top: 2rem;
  width: 24.2rem;
  height: 50rem;
  margin-bottom: 15rem;
  overflow: hidden;

  @media (min-width: 567px) {
    background-size: contain;
    margin-bottom: 0;
  }
`;

const parallaxContent = [
  {
    start: ".parallax-2",
    duration: 400,
    properties: [
      {
        startValue: 200,
        endValue: 0,
        property: 'translateY',
      },
      {
        startValue: 0,
        endValue: 1,
        property: 'opacity',
      },
    ],
  },
];

const PresentationSection = () => {
    const svgString = encodeURIComponent(renderToStaticMarkup(<RoadArrowSvg />));
    const svgImage = `url("data:image/svg+xml,${svgString}")`;

    return (
      <BackgroundImage svgImage={svgImage}>
          <Content className="parallax-2">
            <ParallaxSection parallaxData={parallaxContent}>
              <Heading>
                It's so simple!
              </Heading>
            </ParallaxSection>
            <ParallaxSection parallaxData={parallaxContent}>
              <About>
                With just a few swipes You can easily create workout
                You can always reset your current step or go back to setup with one click.
              </About>
            </ParallaxSection>
          </Content>
            <Images>
              <Phone src="../../images/video.gif" />
              <PhoneFrame src="../../images/phones-2.png" />
            </Images>
      </BackgroundImage>
    )
  }

export default PresentationSection;