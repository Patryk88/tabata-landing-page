import React from 'react';
import { Link } from 'react-router-dom';

import styled from 'styled-components';

import { links } from '../texts/texts.json';

import LogoSvg from '../images/logo-1';

import { colors } from '../styles'

const Container = styled.footer`
    position: relative;
    width: 100%;
    /* height: 20rem; */
    background-color: ${colors.black};

    @media (min-width: 567px) {
        /* padding: 0rem 7rem 0 7rem; */
    }
`;

const Content = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  width: 87.5%;
  height: 100%;
  margin: 0 auto;
  padding: 2rem 0;

  @media (min-width: 567px) {
    flex-direction: row;
  }
`;

const Brand = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  z-index: 1;
`;

const BrandName = styled.h2`
  color: ${colors.yellow};
  text-transform: uppercase;
  font-family: 'rubik-bold-italic';
  font-size: 2rem;

  @media (min-width: 567px) {
    /* display: block; */
  }
`;

const Links = styled.div`
  display: flex;
  flex-direction: column;
  align-self: flex-start;
  padding-top: 2rem;
`;

const Copyright = styled.div`
  align-self: flex-end;
  color: ${colors.yellow};
  font-family: 'rubik-medium';
  font-size: 1.2rem;
`;

const StyledExternalLink = styled.a`
  color: ${colors.white};
  font-family: 'rubik-regular';
  font-size: 1.2rem;
  padding: .5rem;

  :hover {
    color: ${colors.red};
  }
`;

const StyledLink = styled(Link)`
  color: ${colors.white};
  font-family: 'rubik-regular';
  font-size: 1.2rem;
  padding: .5rem;

  :hover {
    color: ${colors.red};
  }
`;

const Footer = () => {
  return (
    <Container>
      <Content>
        <Link to="/">
          <Brand>
            <LogoSvg width={128} />
            <BrandName>Trainingo</BrandName>
          </Brand>
        </Link>
        <Links>
            <StyledLink to="/about">About Trainingo</StyledLink>
            <StyledExternalLink href={links.googlePlay} target="_blank">Training on android</StyledExternalLink>
        </Links>

        <Links>
            <StyledExternalLink href="http://devbrains.pl/" target="_blank">Contact Us</StyledExternalLink>
            <StyledLink to="/privacy-policy">Privacy & policy</StyledLink>
            <StyledLink to="/terms-and-conditions">Terms & conditions</StyledLink>
        </Links>
        <Copyright>
          © Copyright {new Date().getFullYear()}
        </Copyright>
      </Content>
    </Container>
  )
}

export default Footer;